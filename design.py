# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'design.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 500)
        self.messageText = QtWidgets.QTextEdit(Dialog)
        self.messageText.setGeometry(QtCore.QRect(10, 40, 380, 80))
        self.messageText.setObjectName("messageText")
        self.sendButton = QtWidgets.QPushButton(Dialog)
        self.sendButton.setGeometry(QtCore.QRect(10, 130, 380, 40))
        self.sendButton.setObjectName("sendButton")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(20, 20, 121, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(20, 180, 360, 16))
        self.label_2.setObjectName("label_2")
        self.encryptText = QtWidgets.QTextEdit(Dialog)
        self.encryptText.setGeometry(QtCore.QRect(10, 200, 380, 80))
        self.encryptText.setObjectName("encryptText")
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(20, 280, 360, 16))
        self.label_3.setObjectName("label_3")
        self.decryptMessage = QtWidgets.QTextEdit(Dialog)
        self.decryptMessage.setGeometry(QtCore.QRect(10, 300, 380, 80))
        self.decryptMessage.setObjectName("decryptMessage")
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label_4.setGeometry(QtCore.QRect(20, 380, 360, 16))
        self.label_4.setObjectName("label_4")
        self.keys = QtWidgets.QTextEdit(Dialog)
        self.keys.setGeometry(QtCore.QRect(10, 400, 380, 80))
        self.keys.setObjectName("keys")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.sendButton.setText(_translate("Dialog", "Отправить"))
        self.label.setText(_translate("Dialog", "Введите сообщение..."))
        self.label_2.setText(_translate("Dialog", "Зашифрованное сообщение с ключом Alice"))
        self.label_3.setText(_translate("Dialog", "Расшифрованное сообщение с ключом Bob"))
        self.label_4.setText(_translate("Dialog", "Ключи"))
