# генератор простых чисел
from primeGenerator import prime_generator
from math import sqrt
from random import randint

noLess = 10000
noMore = 100000

# Устанавливаем ключи
keys = {
    'public_key_Alice': prime_generator(noLess, noMore),
    'private_key_Alice': prime_generator(noLess, noMore),
    'public_key_Bob': prime_generator(noLess, noMore),
    'private_key_Bob': prime_generator(noLess, noMore),
}

keys['partial_key_Alice'] = keys['public_key_Alice'] ** keys['private_key_Alice'] % keys['public_key_Bob']
keys['partial_key_Bob'] = keys['public_key_Alice'] ** keys['private_key_Bob'] % keys['public_key_Bob']
keys['full_key_Alice'] = keys['partial_key_Bob'] ** keys['private_key_Alice'] % keys['public_key_Bob']
keys['full_key_Bob'] = keys['partial_key_Alice'] ** keys['private_key_Bob'] % keys['public_key_Bob']

# Кодирование простой замены с помощью ключа А
def encrypt(text: str):
    encrypted_text = ''
    for char in text:
        encrypted_text += chr(ord(char) + keys['full_key_Alice'])
    return encrypted_text

# Декодирование с помощью ключа Б
def decrypt(text: str):
    decrypted_text = ''
    for char in text:
        decrypted_text += chr(ord(char) - keys['full_key_Bob'])
    return decrypted_text

# Генерация строки с ключами
def printAllKeys():
    res = ''
    for key, value in keys.items():
        res += key + ' - ' + str(value) + '\n'
    return res
