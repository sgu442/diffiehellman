from PyQt5 import QtWidgets
# сгенерированный интерфейс
from design import Ui_Dialog
import sys

# функции отображения ключей, шифровки, дешифровки
from DiffieHellman import printAllKeys, encrypt, decrypt


class mywindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(mywindow, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        # вешаем событие на кнопку
        self.ui.sendButton.clicked.connect(self.sendButton)
        # устанавливаем сгенерированные ключи
        self.ui.keys.setText(printAllKeys())

    def sendButton(self):
        # получаем сообщение из текстового поля
        messageText = self.ui.messageText.toPlainText()
        # шифруем сообщение с ключом А
        encryptMessage = encrypt(messageText)
        # устанавливаем зашифрованное сообщение в текстовое поле
        self.ui.encryptText.setText(encryptMessage)
        # дешифруем сообщение с ключок Б
        decryptMessage = decrypt(encryptMessage)
        # устанавливаем дешифрованное сообщение в текстовое поле
        self.ui.decryptMessage.setText(decryptMessage)


app = QtWidgets.QApplication([])
application = mywindow()
application.show()

sys.exit(app.exec())
